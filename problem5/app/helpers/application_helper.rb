module ApplicationHelper
  def tabulate_details_of(table_id, table_name, header_cols, objects)
    head_content = ""
    header_cols.each do |col|
      head_content += "<th>"+col+"</th>"
    end
    
    head_content += "<th>actions</th>"
    
    header = "<thead>#{head_content}</thead>"
    footer = "<tfoot>#{head_content}</tfoot"
    
    body_content = ""
    objects.each do |obj|
      row_content = ""
      
      obj.attributes.each do |key, value|
        row_content += "<td>#{value}</td>"
      end
      
      row_content += "<td>#{link_to "edit", eval("edit_#{obj.class.name.downcase}_path(obj)")}||#{link_to "delete", eval("#{obj.class.name.downcase}_path(obj)"), :method => :delete, :data => {:confirm => 'Are you sure you want to DELETE?'}}</td>"
      
      body_content += "<tr>#{row_content}</tr>"
    end
    
    body = "<tbody>#{body_content}</tbody>"
    
    table = "<table id='#{table_id}' name='#{table_name}' style='white-space:nowrap;' border='1'>#{header+body+footer}</table>"
    
    return table
  end
  
  def welcome_text 
    str = "" # if the user has logged in, show the welcome text.
    if current_user 
      str = "Welcome, #{current_user.email} | " 
      str += link_to "Sign Out", sign_out_path
    else 
      str = "#{link_to "Sign In", sign_in_path} | " 
      str += link_to "Sign Up", sign_up_path 
    end 
  end
end
