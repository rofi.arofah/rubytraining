class Product < ActiveRecord::Base
  attr_accessible :name, :price, :stock, :description, :user_id
  
  
  #relations
  belongs_to :user
  has_many :products_categories, :class_name => 'ProductsCategory'
  has_many :categories, :through => :products_categories
  
  
  #validations
  validates :price, :format => {:with => /[0-9]/}
  validates_presence_of :name, :price, :stock, :description, :user_id
  
  
  #scopes
  scope :stock_at_least, lambda{|stock| where("stock >= ?", stock)}
  scope :stock_more_than, lambda{|stock| where("stock > ?", stock)}
  scope :stock_at, lambda{|stock| where("stock = ?", stock)}
  scope :stock_less_than, lambda{|stock| where("stock < ?", stock)}
  scope :stock_at_most, lambda{|stock| where("stock <= ?", stock)}
  
  
  scope :price_at_least, lambda{|price| where("price >= ?", price)}
  scope :price_more_than, lambda{|price| where("price > ?", price)}
  scope :price_at, lambda{|price| where("price = ?", price)}
  scope :price_less_than, lambda{|price| where("price < ?", price)}
  scope :price_at_most, lambda{|price| where("price <= ?", price)}
  scope :price_less_than_1000, where("price<1000")
end
