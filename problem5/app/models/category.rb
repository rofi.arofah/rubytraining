class Category < ActiveRecord::Base
  attr_accessible :name
  
  #relations
  belongs_to :category
  has_many :products_categories, :class_name => 'ProductsCategory'
  has_many :products, :through => :products_categories
  
  #override relations
  has_many :product_name_has_shoes, :through => :products_categories, :source => :product, :class_name => "Product", :foreign_key => "category_id", :conditions => "name like '%shoes%'"
  
  #validations
  validates_presence_of :name
end
