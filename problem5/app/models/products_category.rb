class ProductsCategory < ActiveRecord::Base
  belongs_to :product
  belongs_to :category
  
  scope :has_books, where("name like '%books%'")
end
