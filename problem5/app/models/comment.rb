class Comment < ActiveRecord::Base
  attr_accessible :content, :article_id, :user_id
  
  #relations
  belongs_to :article
  belongs_to :user
  
  #validations
  validates_presence_of :content, :article_id, :user_id
end
