class User < ActiveRecord::Base
  #attr_accessible :first_name, :last_name, :email, :username, :password, :bio_profile, :date_of_birth, :age, :address, :country_id, :password_confirmation
  attr_accessible :email, :password, :age, :address, :password_confirmation
  
  attr_accessor :password
  
  #relations
  belongs_to :country
  has_many :articles
  has_many :comments
  
  #override relations
  has_many :article_title_has_my_country, :class_name => "Article", :foreign_key => "user_id", :conditions => "title like '%my country%'"
  
  #validations
  validates :password, :presence =>{:on => :create}, :confirmation => true
  validates :email, :presence => true, :uniqueness => true
  #validates :first_name, :presence => true, :uniqueness => true, :length => {:minimum => 1, :maximum => 25}
  #validates :last_name, :presence => true, :uniqueness => true, :length => {:minimum => 1, :maximum => 25}
  #validates :country_id, :presence => true
  #validates_presence_of :first_name, :last_name, :email, :username, :password, :bio_profile, :date_of_birth, :age, :address, :country_id
  
  #scopes
  scope :indonesian, where("country_id = 44")
  
  #interceptors
  before_save :encrypt_password
  
  #methods
  def full_address
    "#{self.address}, #{self.country.name}"
  end
  
  def self.age_more_than_18
    self.where("age>18")
  end
  
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end
