class Country < ActiveRecord::Base
  attr_accessible :code, :name
  
  #relations
  has_many :users
  
  #override relations
  has_many :user_indonesian, :class_name => "User", :foreign_key => "country_id", :conditions => "country_id = (select id from countries where lower(name) = 'indonesia')"
  
  #validations
  validates :code, :inclusion => {:in => %w(id usa chn), :message => 'only id, usa, and chn allowed'}
  validates_presence_of :code, :name
end
