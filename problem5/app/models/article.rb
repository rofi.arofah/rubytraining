class Article < ActiveRecord::Base
  attr_accessible :title, :descriptions, :user_id, :rating
  
  #relations
  belongs_to :user
  has_many :comments, :dependent => :destroy
  
  #validations
  validates :title, :presence => true,:exclusion => {:in => %w(nill, empty, blank)}
  validates_presence_of :title, :descriptions, :user_id, :rating
  
  #scopes
  scope :rating_at_least, lambda{|rating| where("rating >= ?", rating)}
  scope :rating_more_than, lambda{|rating| where("rating > ?", rating)}
  scope :rating_at, lambda{|rating| where("rating = ?", rating)}
  scope :rating_less_than, lambda{|rating| where("rating < ?", rating)}
  scope :rating_at_most, lambda{|rating| where("rating <= ?", rating)}
  
  #methods
  #def self.char_more_than_100
  #  self.where("length(descriptions)>100")
  #end
end
