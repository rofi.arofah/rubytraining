class ArticlesController < ApplicationController
  #filters
  before_filter :require_login, :only => [:new, :create, :edit, :update, :delete]
  
  #methods
  def index
    @articles = Article.all
  end
  
  def create
    @article = Article.new
    
    Article.column_names.each do |a|
      @article[a] = params[:article][a]
    end
    
    if @article.save
      flash[:notice] = 'Article creation successful'
      redirect_to :action => :index
    else
      flash[:error] = @article.errors.to_a
      render  :action => :new
    end
  end
  
  def new
    @article = Article.new
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  def update
    @article = Article.find(params[:id])
    
    if @article.update_attributes(params[:article])
      flash[:notice] = 'Article creation successful'
      redirect_to :action => :index
    else
      flash[:error] = @article.errors.to_a
      render  :action => :edit
    end
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    
    redirect_to :action => :index
  end
  
  def show
    @article_id = params[:id]
    @comments = Comment.find_all_by_article_id(params[:id])
    @comment = Comment.new
  end
end
