class CommentsController < ApplicationController
  def index
    @comments = Comment.all
  end
  
  
  #def create 
  #  @comment = Comment.new(params[:comment])
  #  @comment.article = Article.find(params[:comment][:article_id])
  #  @comment.user = current_user
  #  
  #  if @comment.save
  #    flash[:notice] = "Comment creation successful"
  #    redirect_to :action => :index
  #  else
  #    flash[:error] = @comment.errors.to_a
  #    render  :action => :new
  #  end
  #end
  
  def create
    respond_to do |format|
      @comment = Comment.new(params[:comment])
      @comment.user_id = current_user.id 
      #raise @comment.save.inspect
      if @comment.save
        format.html{redirect_to(article_path(article), :notice => 'comment was successfully created. ')}
        format.js{@comments = Article.find(params[:comment][:article_id].to_i).comments}
      end
    end
  end
  
  def new
    @comment = Comment.new
  end
  
  def edit
    @comment = Comment.find(params[:id])
  end
  
  def update
    @comment = Comment.find(params[:id])
    
    if @comment.update_attributes(params[:comment])
      flash[:notice] = 'Comment creation successful'
      redirect_to :action => :index
    else
      flash[:error] = @comment.errors.to_a
      render  :action => :edit
    end
  end
  
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    
    redirect_to :action => :index
  end
  
end
