class CountriesController < ApplicationController
  def index
    @countries = Country.all
  end
  
  def create
    @country = Country.new
    
    Country.column_names.each do |c|
      @country[c] = params[:country][c]
    end
    
    if @country.save
      flash[:notice] = "Country creation successful"
      redirect_to :action => :index
    else
      flash[:error] = @country.errors.to_a
      render :action => :new
    end
  end
  
  def new
    @country = Country.new
  end
  
  def edit
    @country = Country.find(params[:id])
  end
  
  def update
    @country = Country.find(params[:id])
    
    if @country.update_attributes(params[:country])
      flash[:notice] = 'Country edit successful'
      redirect_to :action => :index
    else
      flash[:error] = @country.errors.to_a
      render  :action => :edit
    end
  end
  
  def destroy
    @country = Country.find(params[:id])
    @country.destroy
    
    redirect_to :action => :index
  end
end
