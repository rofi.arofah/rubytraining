class UsersController < ApplicationController
  def index
    @users = User.all
  end
  
  def create
    @user = User.new(params[:user])
    
    if verify_recaptcha
      if @user.save
        UserMailer.registration_confirmation(@user).deliver
        flash[:notice] = "User creation successful"
        redirect_to :action => :index
      else
        flash[:error] = @user.errors.to_a
        render :action => :new
      end
    else
      flash[:error] = "There was an error with the recaptcha code below. Please re-enter the code and click submit. "
      render :action => :new
    end
  end
  
  def new
    @user = User.new
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    
    if @user.update_attributes(params[:user])
      flash[:notice] = 'User creation successful'
      redirect_to :action => :index
    else
      flash[:error] = @user.errors.to_a
      render  :action => :edit
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    
    redirect_to :action => :index
  end
end
