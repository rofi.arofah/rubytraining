class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_user
  
  private 
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  def require_login
    if current_user.nil?
      flash[:error] = "Access denied, please sign in"
      redirect_to sign_in_path
    else
      return current_user
    end
  end
end
