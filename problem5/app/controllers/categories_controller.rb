class CategoriesController < ApplicationController
  def index
    @categories = Category.all
  end
  
  def create
    @category = Category.new
    
    Category.column_names.each do |c|
      @category[c] = params[:category][c]
    end
    
    if @category.save
      flash[:notice] = "Category creation successful"
      redirect_to :action => :index
    else
      flash[:error] = @category.errors.to_a
      render  :action => :new
    end
  end
  
  def new
    @category = Category.new
  end
  
  def edit
    @category = Category.find(params[:id])
  end
  
  def update
    @category = Category.find(params[:id])
    
    if @category.update_attributes(params[:category])
      flash[:notice] = 'Category creation successful'
      redirect_to :action => :index
    else
      flash[:error] = @category.errors.to_a
      render  :action => :edit
    end
  end
  
  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    
    redirect_to :action => :index
  end
end
