class ProductsController < ApplicationController
  def index
    @products = Product.all
  end
  
  def create
    @product = Product.new
    
    Product.column_names.each do |p|
      @product[p] = params[:product][p]
    end
    
    if @product.save
      flash[:notice] = "Product creation successful"
      redirect_to :action => :index
    else
      flash[:error] = @product.errors.to_a
      render :action => :new
    end
  end
  
  def new
    @product = Product.new
  end
  
  def edit
    @product = Product.find(params[:id])
  end
  
  def update
    @product = Product.find(params[:id])
    
    if @product.update_attributes(params[:product])
      flash[:notice] = 'Product edit successful'
      redirect_to :action => :index
    else
      flash[:error] = @product.errors.to_a
      render  :action => :edit
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    
    redirect_to :action => :index
  end
end
