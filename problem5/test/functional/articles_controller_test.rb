require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @article = Article.first
  end
  
  def test_index
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end
  
  def test_new
    get :new
    assert_not_nil assigns(:article)
    assert_response :success
  end
  
  def test_create
    assert_difference('Article.count') do
      article :create, :article => {:title => 'new title1', :descriptions => 'descNew1', :user => User.first, :rating => 5}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).title, 'new title1'
      assert_equal assigns(:article).valid?, true
    end
    
    assert_response :redirect
    assert_redirected_to articles_path(assigns(:article))
    assert_equal flash[:notice], 'Article creation successull'
    
  end
  
  def test_show
    get :show, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end
  
  def test_update
    put :update, :id => Article.first.id, :article =>{:title=>'new title', :descriptions => 'descNew2', :user => User.first, :rating => 5}
    assert_not_nil assigns (:article)
    assert_equal assigns(:article).title, 'new title'
    assert_response :redirect
    assert_redirected_to articles_path(assigns(:article))
    assert_equal flash[:notice], 'Article creation successful'
  end
end
