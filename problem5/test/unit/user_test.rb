require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  
  def test_call_full_address_with_country_and_address
    user=User.first
    user.country=Country.first
    full_address=user.full_address
    assert_not_nil full_address, false
  end
  
  def test_user_age_more_than_18
    users = User.age_more_than_18
    assert_not_nil users, false
  end
  
  
#  def test_user_auth
#    user = User.first
#    user = User.authenticate(params[:email], params[:password])
#    if user
#      session[:user_id] = user.id
#      redirect_to root_url, :notice => "Signed In!!"
#    else
#      flash.now.alert = "Invalid email or password"
#      render :new
#    end
#  end
  
  def test_user_create
    user = User.new
    user.email = 'trialAndError2@mail.com'
    user.password = 'trypassword'
    user.password_confirmation = 'trypassword'
    
    assert_equal user.save, false
  end
end
