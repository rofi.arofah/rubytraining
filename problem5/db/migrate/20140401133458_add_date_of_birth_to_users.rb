class AddDateOfBirthToUsers < ActiveRecord::Migration
  def up
	add_column	:users, :date_of_birth, :string
  end
  
  def down
	remove_column	:users, :date_of_birth
  end
end
