class RelateUsersWithCountry < ActiveRecord::Migration
  def change
    change_table :users do |t|
	  t.belongs_to :country
	end
  end
end
