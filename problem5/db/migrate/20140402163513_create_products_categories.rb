class CreateProductsCategories < ActiveRecord::Migration
  def change
    create_table :products_categories do |t|
      t.belongs_to :product
      t.belongs_to :category
    end
  end
end
