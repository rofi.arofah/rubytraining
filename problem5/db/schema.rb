# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140415182102) do

  create_table "articles", :force => true do |t|
    t.string  "title"
    t.text    "descriptions"
    t.integer "user_id"
    t.integer "rating"
  end

  create_table "categories", :force => true do |t|
    t.string "name"
  end

  create_table "comments", :force => true do |t|
    t.string  "content"
    t.integer "article_id"
    t.integer "user_id"
  end

  create_table "countries", :force => true do |t|
    t.string "code"
    t.string "name"
  end

  create_table "products", :force => true do |t|
    t.string  "name"
    t.string  "price"
    t.string  "stock"
    t.text    "description"
    t.integer "user_id"
  end

  create_table "products_categories", :force => true do |t|
    t.integer "product_id"
    t.integer "category_id"
  end

  create_table "users", :force => true do |t|
    t.string  "first_name"
    t.string  "last_name"
    t.string  "email"
    t.string  "username"
    t.string  "password"
    t.string  "bio_profile"
    t.string  "date_of_birth"
    t.integer "age"
    t.text    "address"
    t.integer "country_id"
    t.string  "password_hash"
    t.string  "password_salt"
    t.boolean "is_admin"
  end

end
