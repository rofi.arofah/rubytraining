class TenMethods
	def methodOne()
	puts "this is method one"
		puts "========================="
end

def methodTwo()
	puts "this is method two"
		puts "========================="
end

def methodThree()
		puts "this is method three"
		puts "========================="
end

def methodFour()
		puts "this is method four"
		puts "========================="
	end

def methodFive()
	puts "this is method five"
		puts "========================="
end

def methodSix(textToModify)
	puts "this is method six"
	puts "text input: #{textToModify}"
		puts "========================="
end

def methodSeven(textToModify)
	puts "this is method seven"
	puts "text input length: #{textToModify.length}"
		puts "========================="
end

def methodEight(textToModify)
	puts "this is method eight"
	puts "text input Capped: #{textToModify.capitalized}"
		puts "========================="
end

def methodNine(textToModify)
	puts "this is method nine"
	puts "text input UPCASE: #{textToModify.upcase}"
		puts "========================="
end

def methodTen(textToModify)
	puts "this is method ten"
	puts "text input reverse: #{textToModify.reverse}"
		puts "========================="
end

end
